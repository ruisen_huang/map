<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/13 0013
 * Time: 02:24
 */
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Hello, World</title>
    <style type="text/css">
        html{height:100%}
        body{height:100%;margin:0px;padding:0px}
        #container{height:500px;}
        .form-group{
            margin-top: 15px;
        }
    </style>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=bQT4s1PZ1eBAltRpnaExVA79">
        //v2.0版本的引用方式：src="http://api.map.baidu.com/api?v=2.0&ak=您的密钥"
        //v1.4版本及以前版本的引用方式：src="http://api.map.baidu.com/api?v=1.4&key=您的密钥&callback=initialize"
    </script>
    <script src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>

</head>

<body>

<div>
    <form role="form" style="overflow: auto;">
        <div class="form-group" >
            <label for="shopLocation" class="col-xs-3 control-label">店铺位置</label>
            <div class="col-xs-9">
                <input type="text" class="form-control" id="shopLocation" >
            </div>

        </div>

        <div class="form-group">
            <label for="lng" class="col-xs-3 control-label">
                经度
            </label>
            <div class="col-xs-9">
                <input type="text" class="form-control" id="lng" >
            </div>

        </div>

        <div class="form-group">
            <label for="lat" class="col-xs-3 control-label">纬度</label>
            <div class="col-xs-9">
                <input type="text" class="form-control" id="lat" >
            </div>

        </div>
        <div class="form-group col-xs-12" >
            <button type="button" class="btn btn-default form-control" id="doPoiSearch">确定</button>
        </div>

    </form>

</div>

<div id="container"></div>
<script type="text/javascript">


        var map = new BMap.Map("container");
//            if (navigator.geolocation) {
//
//                navigator.geolocation.getCurrentPosition(function(position){

        // var point = new BMap.Point(position.coords.longitude, position.coords.latitude);  // 创建点坐标
        var point = new BMap.Point(116.404, 39.915);  // 创建点坐标



                map.centerAndZoom(point, 15);                 // 初始化地图，设置中心点坐标和地图级别
                var marker = new BMap.Marker(point);        // 创建标注
                map.addOverlay(marker);                     // 将标注添加到地图中\
                var opts = {
                    position : point,    // 指定文本标注所在的地理位置
                    offset   : new BMap.Size(10, -30)    //设置文本偏移量
                }
                var name = "我的位置";
                var label = new BMap.Label(name, opts);  // 创建文本标注对象
                label.setStyle({
                    color : "red",
                    fontSize : "12px",
                    height : "20px",
                    lineHeight : "20px",
                    fontFamily:"微软雅黑",
                    border:"none",
                    padding:"0 0"
                });

                map.addOverlay(label);




//                },function(error){
//                    alert("无法获取位置信息，请检查是否已设置打开GPRS");
//                });
//            }else{
//                alert("浏览器不支持定位" );
//            }

    $("#doPoiSearch").click(function(){

        if($("#lat").val() && $("#lng").val()){
            alert("ok");
            $.getJSON("http://api.map.baidu.com/geocoder/v2/?callback=?",
                {
                    location:$("#lat").val()+','+$("#lng").val(),
                    output:"json",
                    pois:1,
                    ak:"bQT4s1PZ1eBAltRpnaExVA79"
                },function(data){
                    if(data.status === 0){
                       $("#shopLocation").val(data.result.formatted_address);
                    }
                    console.log(data);
                });
        }
    });

</script>
</body>
</html>