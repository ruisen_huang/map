<?php
include str_replace('\\','/',dirname(__FILE__))."/includes/map/controller.php";
$dbConfig = include str_replace('\\','/',dirname(__FILE__))."/includes/map/dbconfig.php";
$controller = new Controller($dbConfig);
$controller->doMapRequest();

//    if(!empty($_POST["data"])){
//        $db = new Mysql(array("dbname"=>"map",'host'=>"localhost","port"=>3306,"user"=>"root","passwd"=>"root"));
//        $data = $_POST["data"];
//        $sql = '';
//        foreach($data as $v){
//            $sql .= "INSERT INTO shop (name , address,point) VALUES('".$v['name']."','".$v['address']."',GeomFromText('POINT(".$v['location']['lat']." ".$v['location']['lng'].")'));";
//        }
//
//
//        $db->doSql($sql);
//
//    }
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="http://api.map.baidu.com/library/SearchInfoWindow/1.5/src/SearchInfoWindow_min.css" />
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=bQT4s1PZ1eBAltRpnaExVA79"></script>
    <script src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js">
    </script>
    <script type="text/javascript" src="http://api.map.baidu.com/library/SearchInfoWindow/1.5/src/SearchInfoWindow_min.js"></script>
    <script  src="js/bootstrap.min.js"></script>

    <style type="text/css">

        html {
            height: 100%
        }

        body {
            height: 100%;
            margin: 0px;
            padding: 0px
        }

        #container {
            height: 100%;
            width: 100%;
        }

        .footer {
            position: fixed;
            z-index: 999;
            left: 0;
            right: 0;
            bottom: 0;
            padding: 10px 10px;
            background: #ffffff;
        }

        .header {
           // position: fixed;
            z-index: 999;
            top: 0px;
            left: 0px;
            background: #ffffff;
            width: 100%;
        }

        .search_box {
            background: #39b3d7;
            padding: 5px;
            overflow: auto;
            height: 50px;
        }

        .title_bar {
            text-align: center;
            font-size: x-large;
            height: 40px;
        }

        .cat_list {
            height: 70px;
            border-bottom: solid 2px #dcdcdc;
         display : none;
        }

        .cat_list ul {
            list-style-type: none;
            overflow: auto;
            width: 100%;
            padding: 0px;
            margin: 0px;
        }

        .cat_list li {
            float: left;
            padding: 5px 20px;
            font-size: larger;
            text-align: center;
        }

        .BMapLib_SearchInfoWindow td{
            -webkit-box-sizing: content-box;
            -moz-box-sizing:content-box;
            box-sizing: content-box;


        }


    </style>

</head>
<body>
<div class="header">
    <div class="title_bar">
        <span>酒鬼网</span>
    </div>
    <div class="search_box">
        <div class="col-xs-8" style="position: relative;">
            <input type="text" class="form-control" id="name">

            <div style="position:absolute;top:5px;font-size: 20px;right: 20px;"><a id="searchshop"><i
                        class="glyphicon glyphicon-search"></i></a></div>
        </div>
        <div class="col-xs-4">
            <a class="btn btn-default center-block" id="mapview" href="index.php">
                <i class='glyphicon glyphicon-list'></i>
                <span>附近商家</span>
            </a>
        </div>
    </div>
</div>

<div id="container"></div>


<div class="footer ">
    <div class="cat_show">
        <i class="glyphicon glyphicon-th-list"></i>
        <span>按分类查找</span>
        <i class="glyphicon glyphicon-chevron-right pull-right"></i>
    </div>
    <div>

        <div class="cat_list">
            <ul>
                <?php
                $catList = $controller->getBusinessType();
                $catListStr = '';
                foreach ($catList as $cat) {
                    $catListStr .= "<li catId='{$cat['str_id']}'>{$cat['str_name']}</li>";
                }
                echo $catListStr;
                ?>
            </ul>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">导航</h4>
            </div>
            <div class="modal-body">
                <div class="btn-group btn-group-justified">
                    <a type="button" class="btn btn-default" id="public">公交</a>
                    <a type="button" class="btn btn-default" id="drive">驾车</a>
                    <a type="button" class="btn btn-default" id="walk">步行</a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="clearResult">清除结果</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
</body>


<script type="text/javascript">

    var map = map = new BMap.Map("container",{enableMapClick:false});

    var point = null,toPoint = null;

    //创建检索信息窗口对象
    var content = '<div style="margin:0;line-height:20px;padding:2px;">' +
        '<img src="../img/baidu.jpg" alt="" style="float:right;zoom:1;overflow:hidden;width:100px;height:100px;margin-left:3px;"/>' +
        '地址：北京市海淀区上地十街10号<br/>电话：(010)59928888<br/>简介：百度大厦位于北京市海淀区西二旗地铁站附近，为百度公司综合研发及办公总部。' +
        '</div>';
    var searchInfoWindow = null;
    searchInfoWindow = new BMapLib.SearchInfoWindow(map, content, {
        title:"百度",
        width  : 290,             //宽度
        height : 105,              //高度
        panel  : "panel",         //检索结果面板
        enableAutoPan : true,     //自动平移\
        enableSendToPhone : false,
        searchTypes   :[
            BMAPLIB_TAB_SEARCH,   //周边检索
            BMAPLIB_TAB_TO_HERE,  //到这里去
            BMAPLIB_TAB_FROM_HERE //从这里出发
        ]
    });



    function getShopList(point, catId=0) {

        $.get("Map_NearShop.php", {location: point, catId, catId}, function (data) {
            console.log(data);
            map.clearOverlays();
            map.centerAndZoom(point, 15);                 // 初始化地图，设置中心点坐标和地图级别
            var name = "我的位置";
            var marker = new BMap.Marker(point,{title:name});        // 创建标注
            marker.addEventListener("click", function (data) {
                searchInfoWindow.open(this);
                searchInfoWindow.setTitle(this.getTitle());
            });
            map.addOverlay(marker);                     // 将标注添加到地图中\
            var opts = {
                position: point,    // 指定文本标注所在的地理位置
                offset: new BMap.Size(10, -30)    //设置文本偏移量
            }

            var label = new BMap.Label(name, opts);  // 创建文本标注对象
            label.setStyle({
                color: "red",
                fontSize: "12px",
                height: "20px",
                lineHeight: "20px",
                fontFamily: "微软雅黑",
                border: "none",
                padding: "0 0"
            });

            map.addOverlay(label);

            if (data && data.length != 0) {

                for (var j = 0; j < data.length; j++) {
                    var p = new BMap.Point(data[j].lng, data[j].lat);
                    var name = data[j].supplier_name;
                    var marker = new BMap.Marker(p,{title:name});
                    map.addOverlay(marker);


                    marker.addEventListener("click", function () {
//                        toPoint = this.point;
//                        $("#myModal").modal("show");



                        searchInfoWindow.open(this);

                        searchInfoWindow.setTitle(this.getTitle());
                    });
                    //  map.setCenter(p);
                    var opts = {
                        position: p,    // 指定文本标注所在的地理位置
                        offset: new BMap.Size(10, -30)    //设置文本偏移量
                    }
                    var label = new BMap.Label(name, opts);  // 创建文本标注对象
                    label.setStyle({
                        color: "red",
                        fontSize: "12px",
                        height: "20px",
                        lineHeight: "20px",
                        fontFamily: "微软雅黑",
                        border: "none",
                        padding: "0 0"
                    });

                    map.addOverlay(label);

                }

            }

        }, "json");
    }


    function getAt(lat, lng) {

        point = new BMap.Point(lng, lat);

        map.centerAndZoom(point, 15);                 // 初始化地图，设置中心点坐标和地图级别
        var name = "<?php if(isset($_GET["name"]))echo $_GET["name"]?>";
        var marker = new BMap.Marker(point,{title:name});        // 创建标注
        // 将标注添加到地图中
        var opts = {
            position: point,    // 指定文本标注所在的地理位置
            offset: new BMap.Size(10, -30)    //设置文本偏移量
        }

        var label = new BMap.Label(name, opts);  // 创建文本标注对象
        label.setStyle({
            color: "red",
            fontSize: "12px",
            height: "20px",
            lineHeight: "20px",
            fontFamily: "微软雅黑",
            border: "none",
            padding: "0 0"
        });

        map.addOverlay(label);

        marker.addEventListener("click", function () {
//                        toPoint = this.point;
//                        $("#myModal").modal("show");
            searchInfoWindow.open(this);
            searchInfoWindow.setTitle(this.getTitle());
        });

        map.addOverlay(marker);


    }

    function getNear() {
//            if (navigator.geolocation) {
//
//                navigator.geolocation.getCurrentPosition(function(position){

        // point = new BMap.Point(position.coords.longitude, position.coords.latitude);  // 创建点坐标
        point = new BMap.Point(116.404, 39.915);  // 创建点坐标

        getShopList(point);
//                },function(error){
//                    alert("无法获取位置信息，请检查是否已设置打开GPRS");
//                });
//            }else{
//                alert("浏览器不支持定位" );
//            }


    }


    <?php

     if(isset($_GET["lat"])&&isset($_GET["lng"])){
           echo "getAt({$_GET["lat"]}, {$_GET["lng"]});";
      }else{

         echo "getNear();";
      }

    ?>
    // 添加带有定位的导航控件
    var navigationControl = new BMap.NavigationControl({
        // 靠左上角位置
        anchor: BMAP_ANCHOR_TOP_LEFT,
        // LARGE类型
        type: BMAP_NAVIGATION_CONTROL_LARGE,
        // 启用显示定位
        enableGeolocation: true,
        offset:new BMap.Size(5,100)
    });
    map.addControl(navigationControl);
    // 添加定位控件
    var geolocationControl = new BMap.GeolocationControl({anchor:BMAP_ANCHOR_TOP_RIGHT,offset:new BMap.Size(5,100)});
    geolocationControl.addEventListener("locationSuccess", function(e){
       map.clearOverlays();
        getShopList(e.point);
    });
    geolocationControl.addEventListener("locationError",function(e){
        // 定位失败事件
        alert(e.message);
    });
    map.addControl(geolocationControl);

    $(".cat_show").on("click",function(){


    });
    $(".cat_list").mouseout(function(){

    });

    var startX = 0;
    var startY = 0;

    $(".cat_show")[0].addEventListener('touchstart',function(e){
        //touchmove:触摸进行时，这里的e包含的触摸的元素信息
        startX = e.touches[0].pageX;
        startY = e.touches[0].pageY;
    });

    $(".cat_list")[0].addEventListener('touchstart',function(e){
        //touchmove:触摸进行时，这里的e包含的触摸的元素信息
        startX = e.touches[0].pageX;
        startY = e.touches[0].pageY;
    });

    $(".cat_show")[0].addEventListener('touchmove',function(e){
        //touchmove:触摸进行时，这里的e包含的触摸的元素信息
        var endX = e.touches[0].pageX;
        var endY = e.touches[0].pageY;

        //获取滑动距离
        var distanceX = endX-startX;
        var distanceY = endY-startY;
        //判断滑动方向
        if(Math.abs(distanceX)>Math.abs(distanceY) && distanceX>0){
            console.log('往右滑动');
        }else if(Math.abs(distanceX)>Math.abs(distanceY) && distanceX<0){
            console.log('往左滑动');
        }else if(Math.abs(distanceX)<Math.abs(distanceY) && distanceY<0){
            $(".cat_list").slideDown();
            $(this).hide();
        }else if(Math.abs(distanceX)<Math.abs(distanceY) && distanceY>0){
//
        }else{
            console.log('点击未滑动');
        }

    });

    $(".cat_list")[0].addEventListener('touchmove',function(e){
        //touchmove:触摸进行时，这里的e包含的触摸的元素信息
        var endX = e.touches[0].pageX;
        var endY = e.touches[0].pageY;

        //获取滑动距离
        var distanceX = endX-startX;
        var distanceY = endY-startY;
        //判断滑动方向
        if(Math.abs(distanceX)>Math.abs(distanceY) && distanceX>0){
            console.log('往右滑动');
        }else if(Math.abs(distanceX)>Math.abs(distanceY) && distanceX<0){
            console.log('往左滑动');
        }else if(Math.abs(distanceX)<Math.abs(distanceY) && distanceY<0){
            console.log('往上滑动');
        }else if(Math.abs(distanceX)<Math.abs(distanceY) && distanceY>0){
            $(".cat_show").slideDown();
            $(this).hide();
        }else{
            console.log('点击未滑动');
        }

    });

    $("#searchshop").click(function () {
        var n = $("#name").val();
        if (!n) {
            alert("搜索内容不能为空");
            return;
        }
        map.clearOverlays();
        map.centerAndZoom(point, 15);                 // 初始化地图，设置中心点坐标和地图级别
        var marker = new BMap.Marker(point);        // 创建标注
        marker.addEventListener("click", function () {
            $("#myModal").modal("show");
        });
        map.addOverlay(marker);                     // 将标注添加到地图中\
        var opts = {
            position: point,    // 指定文本标注所在的地理位置
            offset: new BMap.Size(10, -30)    //设置文本偏移量
        }
        var name = "我的位置";
        var label = new BMap.Label(name, opts);  // 创建文本标注对象
        label.setStyle({
            color: "red",
            fontSize: "12px",
            height: "20px",
            lineHeight: "20px",
            fontFamily: "微软雅黑",
            border: "none",
            padding: "0 0"
        });

        map.addOverlay(label);
        $.getJSON("Map_NearShop.php", {n: n, location: point}, function (data) {

            if (data && data.length > 0) {

                for (var i = 0; i < data.length; i++) {
                    var p = new BMap.Point(data[i].lng, data[i].lat);
                    var marker = new BMap.Marker(p);        // 创建标注
                    map.addOverlay(marker);

                    marker.addEventListener("click", function () {
                        $("#myModal").modal("show");
                    });

                    var opts = {
                        position: p,    // 指定文本标注所在的地理位置
                        offset: new BMap.Size(10, -30)    //设置文本偏移量
                    }
                    var name = data[i].supplier_name;
                    var label = new BMap.Label(name, opts);  // 创建文本标注对象
                    label.setStyle({
                        color: "red",
                        fontSize: "12px",
                        height: "20px",
                        lineHeight: "20px",
                        fontFamily: "微软雅黑",
                        border: "none",
                        padding: "0 0"
                    });

                    map.addOverlay(label);
                }
            }
        });
    });

    $(".cat_list li").click(function () {

        catId = $(this).attr("catId");
        getShopList(point, catId);

    });

    var walking = new BMap.WalkingRoute(map, {renderOptions:{map: map, autoViewport: true}});
    var transit = new BMap.TransitRoute(map, {
        renderOptions: {map: map}
    });
    var driving = new BMap.DrivingRoute(map, {renderOptions:{map: map, autoViewport: true}});

    map.addEventListener("click",function(e){


    });

    $("#public").click(function(){


        transit.search(point, toPoint);
    });

    $("#drive").click(function(){

        driving.search(point, toPoint);
    });

    $("#walk").click(function(){


        walking.search(point, toPoint);


    });

    $("#clearResult").click(function(){
        walking.clearResults();
        transit.clearResults();
        driving.clearResults();
    });


</script>
</html>

