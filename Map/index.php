<?php




//if (isset($_GET["location"])) {
//    $location = $_GET["location"];
//    if (isset($_GET["n"])) {
//        echo getShopByName($_GET["n"],$location['lat'], $location['lng']);
//
//    }else if(isset($_GET["start"])){
//        echo getNearShopInfoList($location['lat'], $location['lng'],$_GET["start"],$_GET["num"],$_GET["catId"]);
//    }
//
//    exit;
//
//}

include str_replace('\\','/',dirname(__FILE__))."/includes/map/controller.php";
$dbConfig = include str_replace('\\','/',dirname(__FILE__))."/includes/map/dbconfig.php";
$controller = new Controller($dbConfig);
$controller->doListRequest();

//if(isset($_POST["data"])){
//    $db = new Mysql(array("dbname"=>"map",'host'=>"localhost","port"=>3306,"user"=>"root","passwd"=>"root"));
//    $data = $_POST["data"];
//    $sql = '';
//    foreach($data as $v){
//        $sql .= "INSERT INTO ecs_supplier (supplier_name , address,location,tel,type_id) VALUES('".$v['name']."','".$v['address']."',GeomFromText('POINT(".$v['location']['lat']." ".$v['location']['lng'].")'),'".$v['telephone']."',3);";
//    }
//
//
//    $db->doSql($sql);
//
//}

?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Hello, World</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/iscroll.css">
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=bQT4s1PZ1eBAltRpnaExVA79"></script>
    <script type="text/javascript" src="js/iscroll-probe.js"></script>
    <script src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>


</head>

<body>
<div class="header">
    <div class="title_bar">
        <span>酒鬼网</span>
    </div>
    <div class="search_box">
        <div class="col-xs-8" style="position: relative;">
            <input type="text" class="form-control" id="name">
            <div style="position:absolute;top:5px;font-size: 20px;right: 20px;"><a  id="searchshop"><i class="glyphicon glyphicon-search"></i></a></div>
        </div>
        <div class="col-xs-4">
            <a class="btn btn-default center-block" id="mapview" href="Map_NearShop.php">
                <i class='glyphicon glyphicon-globe'></i>
                <span>地图</span>
            </a>
        </div>

    </div>
    <div class="cat_list">
        <ul>
            <?php
            $catList = $controller->getBusinessType();
            $catListStr = '';
            foreach ($catList as $cat) {
                $catListStr .= "<li catId='{$cat['str_id']}'>{$cat['str_name']}</li>";
            }
            echo $catListStr;
            ?>
        </ul>
    </div>
    <div class="sort_list">
        <ul>
            <li>默认</li>
            <li>销量</li>
            <li>人气</li>
            <li>距离</li>
        </ul>
    </div>

</div>
<div id="MyScroller" class="main">
    <div class="warpper" style="transform: translate(0px, -2122px) translateZ(0px);">
        <div id="PullDown" class="scroller-pullDown" style="display: none;">
            <img style="width: 20px; height: 20px;" src="img/rolling.svg">
            <span id="pullDown-msg" class="pull-down-msg">下拉刷新</span>
        </div>
        <ul id="Content" class="dropdown-list">

        </ul>
        <div id="PullUp" class="scroller-pullUp" style="display: none;">
            <img style="width: 20px; height: 20px;" src="img/rolling.svg">
            <span id="pullUp-msg" class="pull-up-msg">加载更多</span>
        </div>
    </div>
</div>

<div class="footer">

</div>


<script type="text/javascript">

    var myScroll = null,
        start = 1,
        num = 20,
        page = 1,
        point = null,
        catId = 0;


    function getContents(arr) {
        var li = "";
        for (var i = 0; i < arr.length; i++) {
            li += "<li class='media' shopId='"+arr[i].supplier_id+"'><div class='media-left'><img style='width: 50px;height:50px;' src='img/shop.png'></div>"
            + "<div class='media-body'><div><strong>" + arr[i].supplier_name + "</strong></div><div>" + arr[i].address + "</div><div>距离您大约" + arr[i].distance + "米<a class='btn-link map_link' href='Map_NearShop.php?lat=" + arr[i].lat + "&lng=" + arr[i].lng + "&name="+arr[i].supplier_name+"'><i class='glyphicon glyphicon-globe pull-right'></i></a></div></div></li>";
        }
        return li;
    }

    function appendContent(content) {
        var ul = document.getElementById('Content');
        ul.innerHTML = ul.innerHTML + content;
    }

    function getShopList(point, start, num,catId=0) {

        $.getJSON("index.php", {location: point, start: start, num: num,catId,catId}, function (data) {

            if(data.length < num){

                $("#pullUp-msg").text("没有更多了");
            }
            appendContent(getContents(data));
            setTimeout(function () {
                myScroll.refresh();
                page++;
            }, 0);

        });
    }

    window.onload = function () {

        // 初始化body高度
        document.body.style.height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0) + 'px';

         point = new BMap.Point(116.404, 39.915);  // 创建点坐标
        getShopList(point, start, num);

        var pullDown = document.querySelector("#PullDown"),
            pullUp = document.querySelector("#PullUp"),
            isPulled = false,
            isRefreshed = false; // 拉动标记

        setTimeout(function () {
            myScroll = new IScroll('#MyScroller', {
                probeType: 3,
                mouseWheel: true,
                scrollbars: false,
                preventDefault: false,
                fadeScrollbars: false,
                tap: true
            }, 100);
            myScroll.on('scroll', function () {
                var height = this.y,
                    bottomHeight = this.maxScrollY - height;


                // 控制下拉显示
                if (height >= 60) {
                    pullDown.style.display = "block";
                    isRefreshed = true;
                    return;
                }
                else if (height < 60 && height >= 0) {

                    pullDown.style.display = "none";
                    return;
                }

                // 控制上拉显示
                if (bottomHeight >= 60) {
                    pullUp.style.display = "block";
                    isPulled = true;
                    return;
                }
                else if (bottomHeight < 60 && bottomHeight >= 0) {
                    pullUp.style.display = "none";
                    return;
                }
            })

            myScroll.on('scrollEnd', function () { // 滚动结束

                if (isPulled) { // 如果达到触发条件，则执行加载
                    isPulled = false;

                    start = (page - 1) * num + 1;
                    getShopList(point, start, num,catId);
                    myScroll.refresh();

                } else if (isRefreshed) {

                    $("#Content").empty();
                    isRefreshed = false;
                    start = 1;
                    page = 1;
                    getShopList(point, start, num,catId);
                    myScroll.refresh();

                }
            });

            var t1 = null;
            $("#Content").on("tap", 'li', function () {
                if (t1 == null) {
                    t1 = new Date().getTime();
                } else {
                    var t2 = new Date().getTime();
                    if (t2 - t1 < 500) {
                        t1 = t2;
                        return;
                    } else {
                        t1 = t2;
                    }
                }
                location.href="ShopDesc.php?shopId="+$(this).attr("shopId");
            });
            $("#Content").on("tap", '.map_link', function (e) {
                e.preventDefault();//阻止鼠标的默认点击事件
                e.stopImmediatePropagation();//阻止冒泡事件发生
                if (t1 == null) {
                    t1 = new Date().getTime();
                } else {
                    var t2 = new Date().getTime();
                    if (t2 - t1 < 500) {
                        t1 = t2;
                        return;
                    } else {
                        t1 = t2;
                    }
                }

            });


        });


    }

    $("#searchshop").click(function () {
        var n = $("#name").val();
        if (!n) {
            alert("搜索内容不能为空");
            return;
        }
        $.getJSON("index.php", {n: n,location:point}, function (data) {
            $("#Content").empty();
            appendContent(getContents(data));
            setTimeout(function () {
                myScroll.refresh();
            }, 0);
        });
    });

    $(".cat_list li").click(function(){
        $("#Content").empty();
        start = 1;
        page = 1;
        catId = $(this).attr("catId");
        getShopList(point, start, num,catId);
        myScroll.refresh();
    });

</script>

<!--<script>-->
<!---->
<!--   var url =  "http://api.map.baidu.com/place/v2/search?callback=?";-->
<!--   $.getJSON(url,-->
<!--       {-->
<!--           query:"饭店",-->
<!--           scope:2,-->
<!--           output:"json",-->
<!--           location:"39.915,116.404",-->
<!--           radius:5000,-->
<!--           ak:"bQT4s1PZ1eBAltRpnaExVA79",-->
<!--           page_size:20,-->
<!--           page_num:5-->
<!--       },-->
<!--       function(data){-->
<!--           console.log(data);-->
<!--           $.post("index.php",{data:data.results});-->
<!--       }-->
<!--   );-->
<!--</script>-->
</body>
</html>

