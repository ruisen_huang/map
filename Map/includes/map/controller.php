<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/15 0015
 * Time: 23:21
 */
include dirname(__FILE__).DIRECTORY_SEPARATOR."db_handle.php";
include dirname(__FILE__).DIRECTORY_SEPARATOR."lib_map.php";
class Controller {
    private $db = null;
    function __construct($dbConfig){
        $this->db = new Mysql($dbConfig);
    }


    public  function doListRequest(){
        if (isset($_GET["location"])) {
            $location = $_GET["location"];
            if (isset($_GET["n"])) {
                echo getShopByName($this->db,$_GET["n"],$location['lat'], $location['lng']);

            }else if(isset($_GET["start"])){
                echo getNearShopInfoList($this->db,$location['lat'], $location['lng'],$_GET["start"],$_GET["num"],$_GET["catId"]);
            }

            exit;

        }
    }

    public  function  getBusinessType(){
        return getBusinessType($this->db);
    }

    public function doMapRequest(){
        if (isset($_GET["location"])) {
            $location = $_GET["location"];
            if (isset($_GET["n"])) {
                echo getShopByName($this->db,$_GET["n"], $location['lat'], $location['lng']);

            } else if (isset($_GET["catId"])) {
                echo getNearShopInfoList1($this->db,$location['lat'], $location['lng'], $_GET["catId"]);
            } else {
                echo getNearShopInfoList1($this->db,$location['lat'], $location['lng']);
            }

            exit;

        }
    }


    /**
     *  @desc 根据两点间的经纬度计算距离
     *  @param float $lat 纬度值
     *  @param float $lng 经度值
     */
    function getDistance($lat1, $lng1, $lat2, $lng2)
    {
        $earthRadius = 6367000; //approximate radius of earth in meters

        /*
          Convert these degrees to radians
          to work with the formula
        */

        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;

        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;

        /*
          Using the
          Haversine formula

          http://en.wikipedia.org/wiki/Haversine_formula

          calculate the distance
        */

        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);  $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;

        return round($calculatedDistance);
    }
}