<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/7 0007
 * Time: 00:56
 */

/**
 * 获取商家分类
 * @return array
 */
function getBusinessType($db){
    $sql = "select * from ecs_street_category";
    $result = $db->doSql($sql);
    $db->close();
    array_unshift($result,array('str_id'=>0,'str_name'=>'全部'));
    return $result;
}



function getSearchDistance(){
    return 2;
}

function insertShopInfo($db,$data){
    $sql = '';
    foreach($data as $v){
        $sql .= "INSERT INTO ecs_supplier supplier_name , address,location) VALUES('".$v['name']."','".$v['address']."',GeomFromText('POINT(".$v['location']['lat']." ".$v['location']['lng'].")'));";
    }


    $db->doSql($sql);
    $db->close();
}

function getNearShopInfoList($db,$lat,$lng,$start=1,$num=20,$catId=0){
    $dis = getSearchDistance();

    $sql = $catId ? "call proc_getNearShopByCat($lat,$lng,$dis,$start,$num,$catId)":"call proc_getNearShop($lat,$lng,$dis,$start,$num)";
    $result = $db->doSql($sql);
    $db->close();
    return json_encode($result);
}

function getNearShopInfoList1($db,$lat,$lng,$catId=0){
    $dis = getSearchDistance();

    $sql = $catId ? "call proc_getNearShopByCat1($lat,$lng,$dis,$catId)":"call proc_getNearShop1($lat,$lng,$dis)";
    $result = $db->doSql($sql);
    $db->close();
    return json_encode($result);
}

function getShopByName($db,$name,$lat,$lng){

    $sql = "select supplier_name,address,X(location) as lat,Y(location) as lng ,round(6378.138*2*asin(sqrt(pow(sin( ($lat*pi()/180-
           X(location)*pi()/180)/2),2)+cos($lat*pi()/180)*cos(X(location)*pi()/180)* pow(sin( ($lng*pi()/180-Y(location)*pi()/180)/2),
           2)))*1000) as distance from ecs_supplier where supplier_name like '%" . $name . "%' ORDER BY
           (round(6378.138*2*asin(sqrt(pow(sin( ($lat*pi()/180-X(location)*pi()/180)/2),2)+cos($lat*pi()/180)*cos(X(location)*pi()/180)*
            pow(sin( ($lng*pi()/180-Y(location)*pi()/180)/2),2)))*1000)) asc ";
    $result = $db->doSql($sql);
    $db->close();
    return json_encode($result);
}

?>